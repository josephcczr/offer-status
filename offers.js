const request = require('request');

module.exports = {
	modifyOffersRequest: function (req, res, next) {
		console.log('Calling modifyOffersRequest.');

		for(var i = 0;i< req.body.offers.length; i++)
		{
			if (i % 2 == 0) {
				req.body.offers[i].status = 'ACTIVE';
			} else {
				req.body.offers[i].status = 'REDEEMED';
			}
		}
		res.send(req.body.offers);
	}


};
